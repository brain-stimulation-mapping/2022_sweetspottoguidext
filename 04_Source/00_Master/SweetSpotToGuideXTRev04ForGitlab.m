
% Script to transfer nifti sweet spot to Dicom for Brainlab/ GuideXT

% Rev 2 June 2019, Khoa Nguyen, doing a series of ten patients
% Rev 3 June 2019, injecting into sagittal slices
% Rev 3B June 2019; coregistering original t2 sagittal to LeadDBS anat_t23
% that is in MNI coordinates
% BUG: Dec 2019; normalization glanat from Lead DBS 2.2.3 not working; had
% to rerun normalization for one case with version 2.1.6
% SOLVED: need to run the ANT version from Lead-DBS 2.2.3 for those cases;
% still need to investigate more; it adds more slices to the T2 sagittal
% Rev 04, June 2020; update for Debian workstation
% Rev 04*, Feb 2021, update for Lead DBS 2.5.1, normalization warps now
% saved as nii.gz files and no longer .h5

% OVERVIEW: take raw anat_t2, then clear out subcortical area, inject sweet spot
% PREREQUISITE: load Lead DBS, copy patient Dicom file into patient folder
% POSTPROCESSING: DicomBrowser to change file information on DICOM files

%% Preprocessing, find original T2 series
% REVISIT edit folder for each patient
patientFolder = '/03_Data/';

status = copyfile([ patientFolder 'orig_anat_t23.nii' ], [ patientFolder 'orig_anat_t23_workingCopy.nii' ] );
rawAnatT2 = ea_load_nii( [ patientFolder 'orig_anat_t23_workingCopy.nii'] );
MNIAnatT2 = ea_load_nii( [ patientFolder 'anat_t23.nii' ] );
ea_reslice_nii( [ patientFolder 'orig_anat_t23_workingCopy.nii' ], [ patientFolder 'orig_anat_t23_workingCopy.nii' ], rawAnatT2.voxsize, 0, [], 1 ); % LAS to RAS

% rigid body transform to get from scanner coordinates to patient-specific MNI coordinates
flags.cost_fun = 'nmi';
flags.sep = [ 4 2 ];
flags.tol = [ 0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001 ];
flags.fwhm = [ 7 7 ];
fname = [ patientFolder 'orig_anat_t23_workingCopy.nii' ];
fnameMNI = [ patientFolder 'anat_t23.nii' ];
tmp = spm_coreg( [ fnameMNI, ',1' ], [ fname,',1' ], flags );
tmat2 = spm_matrix( tmp(:)' );
anat_t2_vol = spm_vol( fname );
anat_t2_vol.mat = spm_get_space( fname, tmat2\anat_t2_vol.mat );
spm_write_vol( anat_t2_vol, spm_read_vols( anat_t2_vol ) );
rawAnatT2Coreg = ea_load_nii( [ patientFolder 'orig_anat_t23_workingCopy.nii' ] );

%% Sweet spot
% transform sweet spot from MNI template space to patient-specific MNI
% coordinates
sweetSpotFolder = '../../03_Data/';
% for version Lead-DBS v2.3.1
% cmdRight = ['leaddbs231/ext_libs/ANTs/antsApplyTransforms.glnxa64 --verbose 1 --dimensionality 3 --float 1 --input "03_Data/sweetSpot90Percentile.nii" --output "sweetSpotRightNative.nii" --reference-image "' patientFolder 'orig_anat_t23_workingCopy.nii" --transform ["' patientFolder 'glanatInverseComposite.h5",0] --interpolation Linear'];
% cmdLeft = ['leaddbs231/ext_libs/ANTs/antsApplyTransforms.glnxa64 --verbose 1 --dimensionality 3 --float 1 --input "03_Data/sweetSpot90PercentileFlipped.nii" --output "sweetSpotLeftNative.nii" --reference-image "' patientFolder 'orig_anat_t23_workingCopy.nii" --transform ["' patientFolder 'glanatInverseComposite.h5",0] --interpolation Linear']; %NearestNeighbor or linear
% for version 2.5.x
cmdRight = [ '04_Codebase/stable/leaddbs25x/ext_libs/ANTs/antsApplyTransforms.glnxa64 --verbose 1 --dimensionality 3 --float 1 --input "03_Data/sweetSpot90Percentile.nii" --output "sweetSpotRightNative.nii" --reference-image "' patientFolder 'orig_anat_t23_workingCopy.nii" --transform ["' patientFolder 'glanatInverseComposite.nii.gz",0] --interpolation Linear'] ;
cmdLeft = [ '04_Codebase/stable/leaddbs25x/ext_libs/ANTs/antsApplyTransforms.glnxa64 --verbose 1 --dimensionality 3 --float 1 --input "03_Data/sweetSpot90PercentileFlipped.nii" --output "sweetSpotLeftNative.nii" --reference-image "' patientFolder 'orig_anat_t23_workingCopy.nii" --transform ["' patientFolder 'glanatInverseComposite.nii.gz",0] --interpolation Linear' ]; %NearestNeighbor or linear
statusRight = system( cmdRight );
statusLeft = system( cmdLeft );
ea_crop_nii( ea_niigz( 'sweetSpotRightNative.nii' ))
ea_crop_nii( ea_niigz( 'sweetSpotLeftNative.nii' ))
reslice_nii( 'sweetSpotRightNative.nii',[ patientFolder 'sweetSpotRightNativeResliced.nii' ], rawAnatT2Coreg.voxsize, 0, 0, 2 ); % October 2021, minor improvement, non-verbose, 0 background, nearest neighbor interpolation
reslice_nii( 'sweetSpotLeftNative.nii',[ patientFolder 'sweetSpotLeftNativeResliced.nii'] , rawAnatT2Coreg.voxsize, 0, 0, 2 );

% insert transformation here
sweetSpotLeft = ea_load_nii( [ patientFolder 'sweetSpotLeftNativeResliced.nii' ] );
sweetSpotLeft.img = round( sweetSpotLeft.img );
sweetSpotRight = ea_load_nii([ patientFolder 'sweetSpotRightNativeResliced.nii' ]);
sweetSpotRight.img = round( sweetSpotRight.img );

% clear area of subthalamic nucleus and 
emptyVoxels = 6;
[ xx, yy, zz ] = ind2sub( size( sweetSpotRight.img ), find( sweetSpotRight.img > 0 ) ); % 'find' returns linear index, need to convert to subscript
sweetSpotVoxelCoordinates = [ xx, yy, zz ];
sweetSpotWorldCoordinates = mapVoxelToWorld( sweetSpotVoxelCoordinates, sweetSpotRight ); % map from voxel coordinates to millimeter in MNI world coordinates
anatomicalVoxelCoordinates = mapWorldToVoxel( sweetSpotWorldCoordinates, rawAnatT2Coreg ); % map from millimeter world coordinates to voxel coordinates of anatomical T2
minX = min( anatomicalVoxelCoordinates( :, 1 ) );
minY = min( anatomicalVoxelCoordinates( :, 2 ) );
minZ = min( anatomicalVoxelCoordinates( :, 3 ) );
maxX = max( anatomicalVoxelCoordinates( :, 1 ) );
maxY = max( anatomicalVoxelCoordinates( :, 2 ) );
maxZ = max( anatomicalVoxelCoordinates( :, 3 ) );
rawAnatT2Coreg.img( minX-emptyVoxels : maxX + emptyVoxels, minY-emptyVoxels : maxY + emptyVoxels , minZ - emptyVoxels : maxZ + emptyVoxels ) = 0; % clear the surrounding area (1 cm)
linearIdx = sub2ind( size( rawAnatT2Coreg.img ), anatomicalVoxelCoordinates( : , 1 ), anatomicalVoxelCoordinates( :, 2 ),anatomicalVoxelCoordinates( :, 3 ) ); % get the linear index
rawAnatT2Coreg.img(linearIdx) = 750; % need a much higher value than 1

[ xx, yy, zz ] = ind2sub( size( sweetSpotLeft.img ),find( sweetSpotLeft.img > 0 ) ); % 'find' returns linear index, need to convert to subscript
sweetSpotVoxelCoordinates = [ xx, yy, zz ];
sweetSpotWorldCoordinates = mapVoxelToWorld( sweetSpotVoxelCoordinates, sweetSpotLeft ); % map from voxel coordinates to millimeter in MNI world coordinates
anatomicalVoxelCoordinates = mapWorldToVoxel( sweetSpotWorldCoordinates, rawAnatT2Coreg ); % map from millimeter world coordinates to voxel coordinates of anatomical T2
minX = min( anatomicalVoxelCoordinates( :, 1 ) );
minY = min( anatomicalVoxelCoordinates( :, 2 ) );
minZ = min( anatomicalVoxelCoordinates( :, 3 ) );
maxX = max( anatomicalVoxelCoordinates( :, 1 ) );
maxY = max( anatomicalVoxelCoordinates( :, 2 ) );
maxZ = max( anatomicalVoxelCoordinates( :, 3 ) );
rawAnatT2Coreg.img( minX - emptyVoxels : maxX + emptyVoxels, minY - emptyVoxels : maxY + emptyVoxels , minZ - emptyVoxels : maxZ + emptyVoxels ) = 0; % empty the surrounding area (1 cm)
linearIdx = sub2ind( size( rawAnatT2Coreg.img ), anatomicalVoxelCoordinates( :, 1 ), anatomicalVoxelCoordinates( :, 2 ), anatomicalVoxelCoordinates( :, 3 ) ); % get the linear index
rawAnatT2Coreg.img(linearIdx) = 750; % need a much higher value than 1

tmpNifti = make_nii( rawAnatT2Coreg.img, rawAnatT2Coreg.voxsize, mapWorldToVoxel( [ 0 0 0 ], rawAnatT2Coreg ) );
save_nii( tmpNifti, [ patientFolder 'anatT2SagSweetSpot.nii' ] )

%% nifti to dicom
% Rev 4, added env -i at the beginning of the command to run it in the
% Debian shell, not Matlab shell environment
% cmdNiftiToDicom = ['env -i niftitodicom -o ' patientFolder 'sweetSpotSagDicom20190619 ' patientFolder 'anatT2SagSweetSpot20190619.nii ' patientFolder 'PatientT2Sag.dcm --sagittal'];
cmdNiftiToDicom = [ 'env -i niftitodicom -o ' patientFolder 'sweetSpot ' patientFolder 'anatT2SagSweetSpot.nii ' patientFolder 'PatientT2Sag.dcm --sagittal' ];
statusNiftiToDicom = system( cmdNiftiToDicom );

% postprocessing with DicomBrowser, rename protocol and series name;
% call it 't2 sag sweet spot'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% In-line functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function coords = mapVoxelToWorld( XYZ, V )

XYZ = [ XYZ'; ones( 1, size( XYZ, 1 ) )] ;

coords = V.mat * XYZ;
coords = coords( 1:3, : )';
end

function XYZ = mapWorldToVoxel( coords, V )

coords = [ coords'; ones( 1, size( coords, 1 ) ) ];
XYZ = V.mat \ coords;
XYZ = round( XYZ );
% XYZ(XYZ<1) = 1;
XYZ = XYZ( 1:3 , : )';
end
