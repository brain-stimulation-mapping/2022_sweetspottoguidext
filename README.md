# 2022_SweetSpotToGuideXT

Jaradat et al. (2022), Probabilistic Subthalamic Nucleus Stimulation Sweet Spot Integration Into a Commercial Deep Brain Stimulation Programming Software Can Predict Effective Stimulation Parameters, https://doi.org/10.1016/j.neurom.2021.10.026 . 

## Getting started

The sweet spots from Nguyen et al. (2019) are in 03_Data. These represent an effective region for the subthalamic nucleus for rigidity reduction. The script in 04_Source/00_Master requires 

- pre-operative patient images 
- a normalization of the patient from patient-space to MNI space (preferably computed with ANTs inside Lead-DBS)
- VTK-Dicom tools to convert from Nifti file to Dicom 
- DicomBrowser or alike to modify the name of the Dicom images  

The Dicom folder with the sweet spot can then be uploaded into Brainlab Elements and Guide XT as described in the article.
